//
//  PBUserModel.h
//  Pbx
//
//  Created by Macbook Pro on 06.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBUserModel : NSObject

+(void)setKeywordsForUser;
+(NSArray*)getKeywordsForUser;
+(void)createNewUserWithName:(NSString*)name password:(NSString*)password;
+(void)loginUserWithName:(NSString*)name password:(NSString*)password;
@end
