//
//  PBTagsModel.m
//  Pbx
//
//  Created by Macbook Pro on 06.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBTagsModel.h"
#import "PBUserModel.h"
#import <Parse/Parse.h>
@interface PBTagsModel ()

@end
@implementation PBTagsModel
-(void)getTags{
    NSArray* currentUserKeywords = [PBUserModel getKeywordsForUser];
    PFQuery* query = [PFQuery queryWithClassName:@"article"];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query whereKey:@"article_words" containedIn:currentUserKeywords];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            _allArticlesForTags = (NSMutableArray*) objects;
            [self filterTags];
        } else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}
-(void)filterTags{
    NSArray* currentUserKeywords = [PBUserModel getKeywordsForUser];
    NSMutableArray* imagesArray = [NSMutableArray new];
    NSMutableDictionary* filteredArticlesForTags = [NSMutableDictionary new];
    for (NSString* singleUserKeyword in currentUserKeywords) {
        for(PFObject* articleObject in _allArticlesForTags){
            if([[articleObject valueForKeyPath:@"article_words"] containsObject:singleUserKeyword]
            && [[articleObject valueForKeyPath:@"article_photo"] length]>0){
                BOOL identicalImageFound = NO;
                for(NSString* imageString in imagesArray){
                    if([imageString isEqual:[articleObject valueForKeyPath:@"article_photo"]]){
                        identicalImageFound = YES;
                    }
                }
                if(identicalImageFound==NO){
                    filteredArticlesForTags[singleUserKeyword]=articleObject;
                    [_allArticlesForTags removeObject:articleObject];
                    break;
                }
            }
        }
    }
    if(self.delegate && [self.delegate respondsToSelector:@selector(reloadTableViewWithDictionary:)]){
        [self.delegate reloadTableViewWithDictionary:filteredArticlesForTags];
    }
}

@end
