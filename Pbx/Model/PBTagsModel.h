//
//  PBTagsModel.h
//  Pbx
//
//  Created by Macbook Pro on 06.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol PBTagsModelDelegate <NSObject>

- (void)reloadTableViewWithDictionary:(NSDictionary*)data;

@end
@interface PBTagsModel : NSObject

@property (nonatomic, weak)     id<PBTagsModelDelegate> delegate;
@property (nonatomic,strong)    NSMutableArray*allArticlesForTags;

-(void)getTags;

@end
