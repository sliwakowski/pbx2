//
//  PBUserModel.m
//  Pbx
//
//  Created by Macbook Pro on 06.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBUserModel.h"
#import <Parse/Parse.h>
@implementation PBUserModel

+(void)setKeywordsForUser{
    PFUser* currentUser = [PFUser currentUser];
    if (currentUser) {
        // do stuff with the user
    } else {
        [PBUserModel createNewUserWithName:@"test-user" password:@"test-pass"];
        [PBUserModel loginUserWithName:@"test-user" password:@"test-pass"];
        currentUser = [PFUser currentUser];
    }
    NSArray*userKeyword = currentUser[@"keywords"];
    if([userKeyword count]==0) {
        currentUser[@"keywords"] = @[@"facebook", @"apple"];
        [currentUser save];
    }
}
+(NSArray*)getKeywordsForUser{
    PFUser* currentUser = [PFUser currentUser];
    NSArray* keywordsForCurrentUser = currentUser[@"keywords"];
    return keywordsForCurrentUser;
}
+(void)createNewUserWithName:(NSString*)name password:(NSString*)password{
    PFUser *user = [PFUser user];
    user.username = name;
    user.password = password;
    user.email = @"email@example.com";
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            // Hooray! Let them use the app now.
        } else {
            NSString *errorString = [error userInfo][@"error"];
            NSLog(@"PBUserModel:createNewUserWithName: %@", errorString);
        }
    }];
}
+(void)loginUserWithName:(NSString*)name password:(NSString*)password{
    [PFUser logInWithUsernameInBackground:name password:password
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            // Do stuff after successful login.
                                        } else {
                                            // The login failed. Check error to see why.
                                        }
                                    }];
}

@end
