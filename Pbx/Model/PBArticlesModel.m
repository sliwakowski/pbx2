//
//  PBTagsModel.m
//  Pbx
//
//  Created by Macbook Pro on 06.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBArticlesModel.h"
#import "PBUserModel.h"
#import <Parse/Parse.h>
@interface PBArticlesModel ()

@end
@implementation PBArticlesModel
-(void)getArticlesForKeyword:(NSString*)keyword{
    PFQuery* query = [PFQuery queryWithClassName:@"article"];
    [query whereKey:@"article_words" containedIn:@[keyword]];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query setLimit:20];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            _allArticlesForKeyword = objects;
            if(self.delegate && [self.delegate respondsToSelector:@selector(reloadTableViewWithArray:)]){
                [self.delegate reloadTableViewWithArray:_allArticlesForKeyword];
            }
        } else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

@end
