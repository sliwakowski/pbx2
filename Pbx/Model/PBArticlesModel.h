//
//  PBArticlesModel.h
//  Pbx
//
//  Created by Macbook Pro on 08.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol PBArticlesModelDelegate <NSObject>

- (void)reloadTableViewWithArray:(NSArray*)data;

@end
@interface PBArticlesModel : NSObject

@property (nonatomic, weak)     id<PBArticlesModelDelegate> delegate;
@property (nonatomic,strong)    NSArray*allArticlesForKeyword;

-(void)getArticlesForKeyword:(NSString*)keyword;

@end
