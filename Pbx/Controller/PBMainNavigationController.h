//
//  PBMainNavigationController.h
//  Pbx
//
//  Created by Macbook Pro on 06.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <POP/POP.h>
#import "PBTopMenu.h"
#import "PBAddTopic.h"
@interface PBMainNavigationController : UINavigationController <PBTopMenuDelegate, PBAddTopicDelegate>

@property (nonatomic,strong) PBAddTopic *addTopicPopup;
@property (nonatomic,strong) UIView* topMenuOverlay;
@property (nonatomic,strong) IBOutlet UITextField* searchField;
@property (nonatomic,assign) BOOL mainMenuOpened;

@end
