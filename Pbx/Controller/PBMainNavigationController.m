//
//  PBMainNavigationController.m
//  Pbx
//
//  Created by Macbook Pro on 06.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBMainNavigationController.h"
@interface PBMainNavigationController () {
    PBTopMenu*topMenu;
    UIView*topMenuContainer;
    CGPoint previousLocation;
    CGPoint currentLocation;
    int firstX;
    int offsetY;
    float currentTranslation;
    float latestVelocityY;
    float topMenuOffset;
    float topMenuHeight;
    BOOL firstTopMenuOpening;
}

@end

@implementation PBMainNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    firstX = 0;
    offsetY = 0;
    currentTranslation = 0;
    _mainMenuOpened = NO;
    firstTopMenuOpening = YES;
    topMenuOffset = 1000;
    topMenuHeight = 410;
    [self addTopMenu];
    
    UIPanGestureRecognizer* topMenuGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(demoAnimate:)];
    [topMenu addGestureRecognizer:topMenuGesture];
    
    
}
- (void)viewDidLayoutSubviews {
    //_topMenuScrollView.contentSize = CGSizeMake(320, 1020);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (int)calculateTopMenuHeight{
    return 410;
}
- (void)addTopMenu{
    topMenu = [[PBTopMenu alloc] initWithFrame:CGRectMake(0, (-1)*(topMenuHeight+topMenuOffset)+60, 320, topMenuHeight+topMenuOffset)];
    topMenu.delegate = self;
    [self.view addSubview:topMenu];
    
}

- (void)demoAnimate:(UIPanGestureRecognizer*)recognizer
{
    CGPoint velocity = [recognizer velocityInView:self.view];
    CGPoint translation = [recognizer translationInView:recognizer.view];
    velocity.x=0;
    if(translation.y!=0){
        velocity.y = velocity.y/50*(translation.y-currentTranslation);
    }
    if(recognizer.state == UIGestureRecognizerStateChanged){
       // NSLog(@"changed, velocity: %f, roznica %f", velocity.y,translation.y-currentTranslation);
        
        float topPosition = topMenu.center.y+translation.y-currentTranslation;
        topMenu.center = CGPointMake(topMenu.center.x, topPosition);
        latestVelocityY = velocity.y;
        
    }
    else if(recognizer.state == UIGestureRecognizerStateEnded){
        velocity.y = latestVelocityY;
        //NSLog(@"ended, velocity: %f", velocity.y);
        POPSpringAnimation *springAnimation = [POPSpringAnimation animation];
        springAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
        float topPosition = (-1)*(((topMenuOffset+topMenuHeight))/2-60);
        if(!_mainMenuOpened) {
            //topPosition =([self calculateTopMenuHeight])/2;
            //if(firstTopMenuOpening) topPosition-=500;
            //firstTopMenuOpening=NO;
            
            topPosition = (-1)*(((topMenuOffset+topMenuHeight)/2)-topMenuHeight);
        }
        springAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(self.view.bounds.size.width/2, topPosition)];
        springAnimation.velocity = [NSValue valueWithCGPoint:velocity];
        if(velocity.y<0) velocity.y = (-1)*velocity.y;
        float bounceValue = velocity.y/400;
        float bounceSpeed = 0;
        if(bounceValue<1) bounceValue = 1;
        else if(bounceValue>20) bounceValue = 20;
        bounceSpeed = bounceValue;
        if(_mainMenuOpened){
            bounceValue = 2;
            bounceSpeed = 2;
        }
        springAnimation.springSpeed = bounceSpeed;
        springAnimation.springBounciness = bounceValue;
        NSLog(@"Bouciness: %f, springSpeed: %f", bounceValue, bounceValue);
        [topMenu pop_addAnimation:springAnimation forKey:@"center"];
        _mainMenuOpened = !_mainMenuOpened;
        
    }
    
    currentTranslation=translation.y;
}

#pragma mark - TopMenu Delegate
- (void)showAddTopicPopup{
    if(!_addTopicPopup){
        _topMenuOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        _topMenuOverlay.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.6f];
        _topMenuOverlay.alpha = 0;
        [self.view addSubview:_topMenuOverlay];
        POPBasicAnimation *overlayOpacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
        overlayOpacityAnimation.fromValue = @(0);
        overlayOpacityAnimation.toValue = @(1);
        overlayOpacityAnimation.beginTime = CACurrentMediaTime() + 0.1;
        [_topMenuOverlay.layer pop_addAnimation:overlayOpacityAnimation forKey:@"overlayOpacityAnimation"];
        UITapGestureRecognizer* closeAddTopicPopupTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeAddTopicPopupFromSuperview)];
        [_topMenuOverlay addGestureRecognizer:closeAddTopicPopupTapRecognizer];
        
        float popupHeight = 152.0f;
        float popupWidth = 246.0f;
        _addTopicPopup = [[PBAddTopic alloc] initWithFrame:CGRectMake(0, 0, popupWidth, popupHeight)];
        _addTopicPopup.delegate = self;
        _addTopicPopup.center = CGPointMake(self.view.center.x, self.view.center.y);
        _addTopicPopup.alpha = 0;
        [self.view addSubview:_addTopicPopup];
        
        POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
        opacityAnimation.fromValue = @(0);
        opacityAnimation.toValue = @(1);
        opacityAnimation.beginTime = CACurrentMediaTime() + 0.1;
        [_addTopicPopup.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
        
        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
        scaleAnimation.fromValue  = [NSValue valueWithCGSize:CGSizeMake(0.1, 0.1f)];
        scaleAnimation.toValue  = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];//@(0.0f);
        scaleAnimation.springBounciness = 10.0f;
        scaleAnimation.springSpeed = 3.0f;
        [_addTopicPopup.layer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    }
}
- (void)removeAddTopicPopupFromSuperview{
    [_addTopicPopup dismissPopup];
    [_topMenuOverlay removeFromSuperview];
    _topMenuOverlay = nil;
    _addTopicPopup = nil;
}

@end
