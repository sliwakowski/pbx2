//
//  PBSingleArticleVCViewController.h
//  Pbx
//
//  Created by Macbook Pro on 08.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface PBSingleArticleVC : UIViewController <UIWebViewDelegate, UIScrollViewDelegate>

@property (nonatomic,strong) PFObject* articleObject;

@property (nonatomic,strong) NSString* singleArticleObjectId;

@property (nonatomic,weak) IBOutlet UIImageView* tagBackgroundImageView;
@property (nonatomic,weak) IBOutlet UILabel* titleLabel;
@property (nonatomic,weak) IBOutlet UILabel* providerLabel;
@property (nonatomic,weak) IBOutlet UITextView* descriptionTextView;
@property (nonatomic,weak) IBOutlet UIScrollView* scrollView;
@property (nonatomic,weak) IBOutlet UIWebView* contentWebView;
@property (nonatomic,weak) IBOutlet UIImageView* blurredView;

@property (nonatomic,assign)        BOOL isWebViewOpened;
@end
