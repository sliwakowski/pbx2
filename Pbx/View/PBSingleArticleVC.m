//
//  PBSingleArticleVCViewController.m
//  Pbx
//
//  Created by Macbook Pro on 08.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBSingleArticleVC.h"
#import "PBTextHelper.h"
#import "UIImage+ImageEffects.h"
#import <POP/POP.h>
#import <QuartzCore/QuartzCore.h>
@interface PBSingleArticleVC ()

@end

@implementation PBSingleArticleVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
            }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(_articleObject){
        [self setViewValues];
    }
    else {
        PFQuery *query = [PFQuery queryWithClassName:@"article"];
        [query getObjectInBackgroundWithId:_singleArticleObjectId block:^(PFObject *singleArticle, NSError *error) {
            _articleObject = singleArticle;
            [self viewDidLoad];
        }];
    }
}
- (void)setViewValues{
    NSString* imageUrl = [NSString stringWithFormat:@"http://adam.lukaszpiec.pl/timthumb/timthumb.php?src=%@&h=365&w=320&zc=1&cc=000000", _articleObject[@"article_photo"]];
    [_tagBackgroundImageView setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"big-black-placeholder.png"]];
    _titleLabel.text = _articleObject[@"article_title"];
    _providerLabel.text = _articleObject[@"article_provider"];
    _descriptionTextView.text = [NSString stringWithFormat:@"         %@",[PBTextHelper clearTextFromString:_articleObject[@"article_text"]]];
    _descriptionTextView.selectable = NO;
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:_articleObject[@"article_url"]]];
    [_contentWebView loadRequest:urlRequest];
}
- (void)viewDidLayoutSubviews {
    _scrollView.contentSize = CGSizeMake(320, 1020);
    [self createBlurredView];
}
- (void)createBlurredView {
    CGRect screenCaptureRect = [UIScreen mainScreen].bounds;
    UIView *viewWhereYouWantToScreenCapture = [[UIApplication sharedApplication] keyWindow];
    
    //screen capture code
    UIGraphicsBeginImageContextWithOptions(screenCaptureRect.size, NO, [UIScreen mainScreen].scale);
    [viewWhereYouWantToScreenCapture drawViewHierarchyInRect:screenCaptureRect afterScreenUpdates:NO];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage* blurredImage = [capturedImage applyTintEffectWithColor:[UIColor blackColor]];
    /*
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:capturedImage.CGImage];
    
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:10.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *blurredImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    */
    [_blurredView setImage:blurredImage];
    _blurredView.alpha = 0;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSLog(@"offset y: %f",scrollView.contentOffset.y);
    _blurredView.alpha = pow(scrollView.contentOffset.y, 1.0/3)/7;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    NSLog(@"end scroll");
    float topOffset = _isWebViewOpened ? 0 : 508.0f;
    POPSpringAnimation *springAnimation = [POPSpringAnimation animation];
    springAnimation.property = [POPAnimatableProperty propertyWithName:kPOPScrollViewContentOffset];
    springAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(0, topOffset)];
    //springAnimation.velocity = [NSValue valueWithCGPoint:velocity];
    springAnimation.springSpeed = 5;
    springAnimation.springBounciness = 5;
    [_scrollView pop_addAnimation:springAnimation forKey:@"scrollOffset"];
    _isWebViewOpened = !_isWebViewOpened;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
