//
//  PBArticlesTableVC.m
//  Pbx
//
//  Created by Macbook Pro on 05.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//
#import "PBArticlesTableVC.h"
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "PBSingleArticleVC.h"
@interface PBArticlesTableVC ()

@end

@implementation PBArticlesTableVC

#pragma mark - View management
- (void)viewDidLoad
{
    [super viewDidLoad];
    _articlesByTag = [NSArray new];
    [self.tableView setContentInset:UIEdgeInsetsMake(60,0,0,0)];
    PBArticlesModel* articlesModel = [PBArticlesModel new];
    articlesModel.delegate = self;
    [articlesModel getArticlesForKeyword:self.keyword];
    _kArticleCell = @"articleCell";
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_articlesByTag count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:_kArticleCell forIndexPath:indexPath];
    PFObject* articleForTag = [_articlesByTag objectAtIndex:indexPath.row];
    
    UIImageView* tagBackgroundImageView = (UIImageView*) [cell viewWithTag:1];
    NSString* imageUrl = [NSString stringWithFormat:@"http://adam.lukaszpiec.pl/timthumb/timthumb.php?src=%@&h=730&w=640&zc=1&cc=000000", articleForTag[@"article_photo"]];
    NSLog(@"%@", imageUrl);
    [tagBackgroundImageView setImageWithURL:[NSURL URLWithString:imageUrl]
                           placeholderImage:[UIImage imageNamed:@"big-black-placeholder.png"]];
    
    UILabel* titleLabel = (UILabel*) [cell viewWithTag:2];
    titleLabel.text = articleForTag[@"article_title"];
    
    UILabel* providerLabel = (UILabel*) [cell viewWithTag:3];
    providerLabel.text = articleForTag[@"article_provider"];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 365.0f;
}
#pragma mark - Get Tags Delegate
- (void)reloadTableViewWithArray:(NSArray*)data{
    NSLog(@"Do reload");
    _articlesByTag = data;
    [self.tableView reloadData];
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     if ([segue.identifier isEqualToString:@"showSingleArticle"]) {
         UITableViewCell* cell = (UITableViewCell *) sender;
         NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
         PFObject* articleForTag = [_articlesByTag objectAtIndex:indexPath.row];
         
         PBSingleArticleVC* singleArticleVC = (PBSingleArticleVC*) [segue destinationViewController];
         singleArticleVC.articleObject = articleForTag;
     }
 }

#pragma mark - Class methods
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
