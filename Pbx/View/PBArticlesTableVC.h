//
//  PBArticlesTableVC.h
//  Pbx
//
//  Created by Macbook Pro on 05.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBArticlesModel.h"
@interface PBArticlesTableVC : UITableViewController <PBArticlesModelDelegate>

@property (nonatomic,strong) NSString* keyword;
@property (nonatomic,strong) NSString* searchKeyword;
@property (nonatomic,strong) NSString* kArticleCell;
@property (nonatomic,strong) NSArray* articlesByTag;
@end
