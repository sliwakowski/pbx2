//
//  PBTagsTableVC.h
//  Pbx
//
//  Created by Macbook Pro on 05.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBTagsModel.h"
@interface PBTagsTableVC : UITableViewController <UIGestureRecognizerDelegate, PBTagsModelDelegate>

@property (nonatomic,strong) NSString* kTagCell;
@property (nonatomic,strong) NSString* singleArticleObjectId;
@property (nonatomic,strong) NSString* searchKeyword;
@property (nonatomic,strong) NSDictionary* articlesByTags;
@property (nonatomic,strong) NSArray* articlesTags;
@property (nonatomic,strong) PBTagsModel* tagsModel;
-(void)reloadTableAfterDeletingKeyword:(NSArray*)newKeywordsArray;
@end
