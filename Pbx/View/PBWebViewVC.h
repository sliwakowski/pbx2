//
//  PBWebViewVC.h
//  Pbx
//
//  Created by Macbook Pro on 11.06.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PBWebViewVC : UIViewController

@property (nonatomic, strong) NSString*                 urlToLoad;
@property (nonatomic, strong) IBOutlet UIWebView*       webView;

@end
