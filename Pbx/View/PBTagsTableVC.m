//
//  PBTagsTableVC.m
//  Pbx
//
//  Created by Macbook Pro on 05.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBTagsTableVC.h"

#import "MZFormSheetController.h"
#import "MZCustomTransition.h"
#import "PBPopupTableVC.h"
#import "MZFormSheetSegue.h"
#import "PBArticlesTableVC.h"
#import <Parse/Parse.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import "PBSingleArticleVC.h"
#import "PBWebViewVC.h"
@interface PBTagsTableVC ()
@end

@implementation PBTagsTableVC

#pragma mark - View management
- (void)viewDidLoad
{
    [super viewDidLoad];
    _kTagCell = @"tagCell";
    //[[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    //[[MZFormSheetBackgroundWindow appearance] setBlurRadius:2.0];
    [[MZFormSheetBackgroundWindow appearance] setBackgroundColor:[UIColor clearColor]];
    [MZFormSheetController registerTransitionClass:[MZCustomTransition class] forTransitionStyle:MZFormSheetTransitionStyleCustom];
    _articlesByTags = [NSDictionary new];
    [self.tableView setContentInset:UIEdgeInsetsMake(60,0,0,0)];
    _tagsModel = [PBTagsModel new];
    _tagsModel.delegate = self;
    [_tagsModel getTags];
    
    //Refresh Tags Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshTagsNotification:)
                                                 name:@"RefreshTags"
                                               object:nil];
    //Back to home Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(backHome:)
                                                 name:@"BackHome"
                                               object:nil];
    //Search for keyword Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(searchForKeyword:)
                                                 name:@"SearchForKeyword"
                                               object:nil];
    //Open webpage Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openWebpage:)
                                                 name:@"OpenWebpage"
                                               object:nil];
    //Handle Push - Open single article Notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openSingleArticle:)
                                                 name:@"OpenSingleArticle"
                                               object:nil];

}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidAppear:(BOOL)animated{
    if([_searchKeyword length]>0){
        [self performSegueWithIdentifier:@"showArticles" sender:self];
    }
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_articlesTags count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:_kTagCell forIndexPath:indexPath];
    NSString* tagName = [_articlesTags objectAtIndex:indexPath.row];
    PFObject* articleForTag = [_articlesByTags objectForKey:tagName];
    
    UIImageView* tagBackgroundImageView = (UIImageView*) [cell viewWithTag:1];
    NSString* imageUrl = [NSString stringWithFormat:@"http://adam.lukaszpiec.pl/timthumb/timthumb.php?src=%@&h=320&w=640&zc=1&cc=000000", articleForTag[@"article_photo"]];
    [tagBackgroundImageView setImageWithURL:[NSURL URLWithString:imageUrl]
                   placeholderImage:[UIImage imageNamed:@"wolf.png"]];
    
    UILabel* tagNameLabel = (UILabel*) [cell viewWithTag:2];
    tagNameLabel.text = [tagName capitalizedString];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 160.0;
}
#pragma mark - Get Tags Delegate
- (void) refreshTagsNotification:(NSNotification *) notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSArray *userKeywords = [userInfo objectForKey:@"userKeywords"];
    
    [_tagsModel getTags];
    [self reloadTableAfterDeletingKeyword:userKeywords];
    
}
- (void)reloadTableViewWithDictionary:(NSDictionary*)data{
    NSLog(@"Do reload");
    _articlesByTags = data;
    _articlesTags = [data allKeys];
    [self.tableView reloadData];
}
-(void)reloadTableAfterDeletingKeyword:(NSArray*)newKeywordsArray{
    //PBTagsModel* tagsModel = [PBTagsModel new];
    //tagsModel.delegate = self;
    _articlesTags = newKeywordsArray;
    [self.tableView reloadData];
}

#pragma mark - Menu handler
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"formSheet"]) {
        UITableViewCell *cell = (UITableViewCell *)[[[(UIView *)sender superview] superview] superview];
        NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
        NSString* selectedKeyword = [_articlesTags objectAtIndex:indexPath.row];
        
        MZFormSheetSegue *formSheetSegue = (MZFormSheetSegue *)segue;
        PBPopupTableVC* popupView = (PBPopupTableVC*) [segue destinationViewController];
        NSLog(@"a: %@", selectedKeyword);
        popupView.keyword = selectedKeyword;
        popupView.tagsTableVC = self;
        MZFormSheetController *formSheet = formSheetSegue.formSheetController;
        formSheet.transitionStyle = MZFormSheetTransitionStyleBounce;
        formSheet.cornerRadius = 8.0;
        formSheet.presentedFormSheetSize = CGSizeMake(246, 450);
        formSheet.borderColor = [UIColor colorWithRed:34.0f/255.0f green:34.0f/255.0f blue:34.0f/255.0f alpha:1];
        formSheet.didTapOnBackgroundViewCompletionHandler = ^(CGPoint location) {};
        formSheet.shouldDismissOnBackgroundViewTap = YES;
        formSheet.didPresentCompletionHandler = ^(UIViewController *presentedFSViewController) {};
    }
    else if ([segue.identifier isEqualToString:@"showArticles"]) {
        if([_searchKeyword length]<1){
            UITableViewCell* cell = (UITableViewCell *) sender;
            NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
            _searchKeyword = [_articlesTags objectAtIndex:indexPath.row];
        }
        PBArticlesTableVC* articlesTableVC = (PBArticlesTableVC*) [segue destinationViewController];
        articlesTableVC.keyword = _searchKeyword;
        _searchKeyword = nil;
    }
    else if ([segue.identifier isEqualToString:@"showSingleArticle"]) {
        PBSingleArticleVC* singleArticleVC = (PBSingleArticleVC*) [segue destinationViewController];
        singleArticleVC.singleArticleObjectId = _singleArticleObjectId;
    }
}

#pragma mark - Push handler
- (void)openSingleArticle:(NSNotification *) notification{
    NSDictionary *userInfo = notification.userInfo;
    _singleArticleObjectId= [userInfo objectForKey:@"objectId"];
    [self performSegueWithIdentifier:@"showSingleArticle" sender:self];
}

#pragma mark - Search for keyword
- (void)searchForKeyword:(NSNotification *) notification{
    NSDictionary *userInfo = notification.userInfo;
    _searchKeyword = [userInfo objectForKey:@"searchKeyword"];
    UIViewController *lastViewController = [[self.navigationController viewControllers] lastObject];
    
    if([lastViewController isKindOfClass:[PBTagsTableVC class]]) {
        [self performSegueWithIdentifier:@"showArticles" sender:self];
    }
    else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}
#pragma mark - Open webpage
- (void)openWebpage:(NSNotification *) notification{
    NSDictionary *userInfo = notification.userInfo;
    NSString* urlToLoad= [userInfo objectForKey:@"urlToLoad"];
    UIViewController *lastViewController = [[self.navigationController viewControllers] lastObject];
    
    if([lastViewController isKindOfClass:[PBTagsTableVC class]]) { }
    else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    PBWebViewVC* webViewController = [PBWebViewVC new];
    webViewController.urlToLoad = urlToLoad;
    [self.navigationController pushViewController:webViewController animated:YES];
}

#pragma mark - Class methods
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)backHome:(NSNotification *) notification{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
