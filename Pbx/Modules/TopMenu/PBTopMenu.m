//
//  PBTopMenu.m
//  Pbx
//
//  Created by Macbook Pro on 24.04.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBTopMenu.h"
#import <POP/POP.h>
#import <QuartzCore/QuartzCore.h>
@implementation PBTopMenu{
    float topOffset;
    float buttonHeight;
    float topMenuOffset;
    float topMenuHeight;
}



#pragma mark - Init methods
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        topOffset = 1000;
        buttonHeight = 50;
        topMenuOffset = 1000;
        topMenuHeight = 410;
        _topMenuClosed = YES;
        [self initTopMenu];
        
    }
    return self;
}
#pragma mark - View elements creation
- (void)initTopMenu
{
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.95];
    _settingsArray = (NSMutableArray*) @[@"Home", @"Add topic", @"Mute notifications", @"Send feedback", @"Help", @"About", @"Log out"];
    for(NSString* buttonTitle in _settingsArray){
        [self createListButtonWitTitle:buttonTitle];
    }
    [self createSearchBar];
}
- (UIButton*) createListButtonWitTitle:(NSString*)title{
    UIButton* listButton = [UIButton buttonWithType:UIButtonTypeSystem];
    listButton.frame = CGRectMake(15, topOffset, 290, buttonHeight);
    [listButton setTitle:title forState:UIControlStateNormal];
    [listButton setTintColor:[UIColor whiteColor]];
    [listButton.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
    listButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    NSString* methodName = [[title lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
    SEL selector = NSSelectorFromString(methodName);
    if([self respondsToSelector:selector]){
        [listButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        NSLog(@"Selector does not exist");
    }
    topOffset+=buttonHeight;
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, topOffset-1, 320, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.15f alpha:1.0f].CGColor;
    [self.layer addSublayer:bottomBorder];
    [self addSubview:listButton];
    return listButton;
}
- (UIView*) createSearchBar
{
    //SearchBar
    UIView* searchBar = [[UIView alloc] initWithFrame:CGRectMake(0, topOffset, 320, 60)];
    [self addSubview:searchBar];
    //SearchIcon
    UIImageView* searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 22, 16, 16)];
    [searchIcon setImage:[UIImage imageNamed:@"loupe.png"]];
    [searchBar addSubview:searchIcon];
    //SearchField
    UITextField* searchField = [[UITextField alloc] initWithFrame:CGRectMake(50, 15, 213, 30)];
    searchField.textColor = [UIColor colorWithRed:114.0f/255.0f green:114.0f/255.0f blue:114.0f/255.0f alpha:1];
    searchField.clearsOnBeginEditing = YES;
    searchField.text = @"Search for 'bmw i8'";
    [searchField setFont:[UIFont systemFontOfSize:14.0f]];
    searchField.returnKeyType = UIReturnKeySearch;
    searchField.delegate = self;
    [searchBar addSubview:searchField];
    //MenuIcon
    UIImageView* menuIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu.png"]];
    menuIcon.frame =CGRectMake(289, 23, 16, 13);
    [searchBar addSubview:menuIcon];
    UIView* menuIconResponder = [[UIView alloc] initWithFrame:CGRectMake(270, 0, 50, 60)];
    menuIconResponder.backgroundColor = [UIColor clearColor];
    [searchBar addSubview:menuIconResponder];
    UITapGestureRecognizer* openMenuGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleTopMenu)];
    [menuIconResponder addGestureRecognizer:openMenuGesture];
    return searchBar;
}
#pragma mark - Menu Frame
- (int)calculateTopMenuHeight{
    return 410+1000;
}
- (void)openTopMenu{
    
    float topMenuTopOffset = (-1)*(((topMenuOffset+topMenuHeight)/2)-topMenuHeight);
    POPSpringAnimation *springAnimation = [POPSpringAnimation animation];
    springAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
    springAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(self.bounds.size.width/2, topMenuTopOffset)];
    //springAnimation.velocity = [NSValue valueWithCGPoint:velocity];
    springAnimation.springSpeed = 5;
    springAnimation.springBounciness = 5;
    [self pop_addAnimation:springAnimation forKey:@"center"];
    _topMenuClosed = NO;
}
- (void)closeTopMenu{
    float topMenuTopOffset = (-1)*(((topMenuOffset+topMenuHeight))/2-60);
    POPSpringAnimation *springAnimation = [POPSpringAnimation animation];
    springAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
    springAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(self.bounds.size.width/2, topMenuTopOffset)];
    //springAnimation.velocity = [NSValue valueWithCGPoint:velocity];
    springAnimation.springSpeed = 5;
    springAnimation.springBounciness = 5;
    [self pop_addAnimation:springAnimation forKey:@"center"];
    _topMenuClosed = YES;
}
- (void)toggleTopMenu{
    if(_topMenuClosed){
        [self openTopMenu];
    } else {
        [self closeTopMenu];
    }
}
#pragma mark - Menu Methods
- (void)home{
    [self closeTopMenu];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"BackHome" object:nil userInfo:nil];
}
- (void)addtopic{
    [self closeTopMenu];
    [_delegate showAddTopicPopup];
}
- (void)help{
    [self closeTopMenu];
    NSDictionary*webpageUserInfo = [NSDictionary dictionaryWithObject:@"http://www.onet.pl" forKey:@"urlToLoad"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"OpenWebpage" object:nil userInfo:webpageUserInfo];
}
- (void)about{
    [self closeTopMenu];
    NSDictionary*webpageUserInfo = [NSDictionary dictionaryWithObject:@"http://www.onet.pl" forKey:@"urlToLoad"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"OpenWebpage" object:nil userInfo:webpageUserInfo];
}
#pragma mark - Text Field Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self closeTopMenu];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    NSLog(@"did begin");
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"should return");
    //[[NSNotificationCenter defaultCenter] postNotificationName: @"BackHome" object:nil userInfo:nil];
    
    NSDictionary*searchUserInfo = [NSDictionary dictionaryWithObject:[textField.text lowercaseString] forKey:@"searchKeyword"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"SearchForKeyword" object:nil userInfo:searchUserInfo];
    [textField resignFirstResponder];
    return YES;
}
@end
