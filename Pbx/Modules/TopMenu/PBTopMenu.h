//
//  PBTopMenu.h
//  Pbx
//
//  Created by Macbook Pro on 24.04.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PBTopMenuDelegate <NSObject>

- (void)showAddTopicPopup;

@end
@interface PBTopMenu : UIView <UITextFieldDelegate>

@property (nonatomic, weak)  id<PBTopMenuDelegate> delegate;
@property (nonatomic,strong) UIView* topMenuContainer;
@property (nonatomic,strong) NSMutableArray*settingsArray;
@property (nonatomic,assign) BOOL topMenuClosed;

@end
