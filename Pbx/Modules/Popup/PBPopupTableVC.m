//
//  PBPopupTableVC.m
//  Pbx
//
//  Created by Macbook Pro on 05.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBPopupTableVC.h"
#import <Parse/Parse.h>

#import "MZFormSheetController.h"
@interface PBPopupTableVC ()

@end

@implementation PBPopupTableVC
@synthesize kPopupCell = _kPopupCell;
@synthesize popupListItems = _popupListItems;

#pragma mark - View management
- (void)viewDidLoad
{
    [super viewDidLoad];
    _kPopupCell = @"popupCell";
    _popupListItems = (NSMutableArray*) @[@"Change topic", @"Remove topic", @"Notifications", @"All", @"Up to 5 times a day", @"Once a day", @"Mute"];
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidAppear:(BOOL)animated{
    _titleLabel.text = [_keyword capitalizedString];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_popupListItems count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    int selectedOption = indexPath.row;
    switch (selectedOption)
    {
        case 0:
            
            break;
        case 1:
            [self removeTopic];
            break;
            
        case 2:
            
            break;
        default:
            
            break;
    }
    NSLog(@"Selected: %i", selectedOption);
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:_kPopupCell forIndexPath:indexPath];
    UILabel* titleLabel = (UILabel*)[cell viewWithTag:1];
    titleLabel.text = [_popupListItems objectAtIndex:indexPath.row];
    
    UIView *myBackView = [[UIView alloc] initWithFrame:cell.frame];
    myBackView.backgroundColor = [UIColor colorWithRed:0.08 green:0.08 blue:0.08 alpha:1];
    cell.selectedBackgroundView = myBackView;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}
#pragma mark - Settings
- (void)removeTopic{
    PFUser* currentUser = [PFUser currentUser];
    NSMutableArray*userKeywords = (NSMutableArray*) currentUser[@"keywords"];
    [userKeywords removeObject:_keyword];
    currentUser[@"keywords"] = userKeywords;
    [currentUser save];
    NSLog(@"Usunięto: %@", _keyword);
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        _tagsTableVC.articlesTags = userKeywords;
        [_tagsTableVC reloadTableAfterDeletingKeyword:userKeywords];
    }];
}
#pragma mark - Class methods
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end