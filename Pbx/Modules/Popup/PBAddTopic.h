//
//  PBAddTopic.h
//  Pbx
//
//  Created by Macbook Pro on 21.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PBAddTopicDelegate <NSObject>

- (void)removeAddTopicPopupFromSuperview;

@end
@interface PBAddTopic : UIView <UITextFieldDelegate>

@property (nonatomic, weak)  id<PBAddTopicDelegate> delegate;
@property (nonatomic,strong) UITextField* inputField;
- (void)dismissPopup;

@end
