//
//  PBAddTopic.m
//  Pbx
//
//  Created by Macbook Pro on 21.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBAddTopic.h"
#import <QuartzCore/QuartzCore.h>
#import <Parse/Parse.h>
#import <POP/POP.h>
@implementation PBAddTopic {
    float popupHeight;
    float popupWidth;
    float elementTopOffset;
    int keywordChangedCounter;
    BOOL keywordChanged;
    NSString* inputFieldPlaceholder;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        popupHeight = 152.0f;
        popupWidth = 246.0f;
        elementTopOffset = 0;
        inputFieldPlaceholder =  @"You must type something";
        [self initPopup];
        [self addViews];
    }
    return self;
}

- (void)initPopup{
    float offsetLeft = (self.bounds.size.width-popupWidth)/2;
    float offsetTop = (self.bounds.size.height-popupHeight)/2;
    keywordChangedCounter = 0;
    //self.frame = CGRectMake(offsetLeft, offsetTop, popupWidth, popupHeight);
    self.backgroundColor = [UIColor colorWithRed:7.0f/255.0f green:7.0f/255.0f blue:7.0f/255.0f alpha:1];
    self.layer.cornerRadius = 8.0f;
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [UIColor colorWithRed:58.0f/255.0f green:58.0f/255.0f blue:58.0f/255.0f alpha:1].CGColor;
}
- (void)addViews{
    
    UIView* titleViewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, popupWidth, 50.0f)];
    [self addSubview:titleViewContainer];
    elementTopOffset+=titleViewContainer.frame.size.height;
    //Title label
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, popupWidth, 30)];
    titleLabel.text = @"Add topic";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLabel];
    //Text field
    _inputField = [[UITextField alloc] initWithFrame:CGRectMake(0, elementTopOffset+10, popupWidth, 30)];
    _inputField.textColor = [UIColor colorWithRed:114.0f/255.0f green:114.0f/255.0f blue:114.0f/255.0f alpha:1];
    _inputField.clearsOnBeginEditing = YES;
    _inputField.text = @"Type your keyword";
    _inputField.returnKeyType = UIReturnKeyDone;
    _inputField.delegate = self;
    _inputField.font = [UIFont systemFontOfSize:14.0f];
    _inputField.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_inputField];
    elementTopOffset+=(_inputField.frame.size.height+20);
    //Accept Button
    UIButton* acceptButton = [UIButton buttonWithType:UIButtonTypeSystem];
    acceptButton.frame = CGRectMake(0, elementTopOffset, popupWidth, 50.0f);
    [acceptButton setTitle:@"Accept" forState:UIControlStateNormal];
    [acceptButton setTintColor:[UIColor whiteColor]];
    [acceptButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    [acceptButton addTarget:self action:@selector(acceptKeyword) forControlEvents:UIControlEventTouchUpInside];
    acceptButton.backgroundColor = [UIColor colorWithRed:28.0f/255.0f green:28.0f/255.0f blue:28.0f/255.0f alpha:1];
    [self addSubview:acceptButton];
    //Border top
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0.0f, 49, popupWidth, 1.0f);
    topBorder.backgroundColor = [UIColor colorWithRed:58.0f/255.0f green:58.0f/255.0f blue:58.0f/255.0f alpha:1].CGColor;
    [self.layer addSublayer:topBorder];
    //Border bottom
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, 99.0f, popupWidth, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithRed:58.0f/255.0f green:58.0f/255.0f blue:58.0f/255.0f alpha:1].CGColor;
    [self.layer addSublayer:bottomBorder];
    
}

-(void)acceptKeyword{
    if(keywordChangedCounter>0 && _inputField.text.length>2 && keywordChanged && ![_inputField.text isEqual:inputFieldPlaceholder])
    {
        NSLog(@"accept");
        NSString* keyword = _inputField.text;
        PFUser* currentUser = [PFUser currentUser];
        NSMutableArray*userKeywords = (NSMutableArray*) currentUser[@"keywords"];
        [userKeywords addObject:[keyword lowercaseString]];
        currentUser[@"keywords"] = userKeywords;
        [currentUser save];
        NSLog(@"Dodano: %@", keyword);
        
        NSDictionary *userKeywordsInfo = [NSDictionary dictionaryWithObject:userKeywords forKey:@"userKeywords"];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"RefreshTags" object:nil userInfo:userKeywordsInfo];
        [_inputField resignFirstResponder];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"BackHome" object:nil userInfo:nil];
        [self.delegate removeAddTopicPopupFromSuperview];
    }
    else if(keywordChangedCounter>1 && ((_inputField.text.length<=2) || [_inputField.text isEqual:inputFieldPlaceholder])){
        [self.delegate removeAddTopicPopupFromSuperview];
        [_inputField resignFirstResponder];
    }
    else {
        _inputField.text = inputFieldPlaceholder;
        keywordChangedCounter+=2;
    }
}
#pragma mark - Text Field Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"should begin");
    
    float topMenuTopOffset = self.center.y-50.0f;
    POPSpringAnimation *springAnimation = [POPSpringAnimation animation];
    springAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
    springAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(self.center.x, topMenuTopOffset)];
    springAnimation.springSpeed = 5;
    springAnimation.springBounciness = 5;
    [self pop_addAnimation:springAnimation forKey:@"center"];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    NSLog(@"did begin");
    keywordChanged = YES;
    keywordChangedCounter++;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self acceptKeyword];
    return YES;
}
#pragma mark - Popup methods
- (void)dismissPopup{
    POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation.fromValue = @(1);
    opacityAnimation.toValue = @(0);
    opacityAnimation.beginTime = CACurrentMediaTime() + 0.1;
    [self.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.fromValue  = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
    scaleAnimation.toValue  = [NSValue valueWithCGSize:CGSizeMake(0, 0)];//@(0.0f);
    scaleAnimation.springBounciness = 10.0f;
    scaleAnimation.springSpeed = 3.0f;
    [self.layer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}


@end
