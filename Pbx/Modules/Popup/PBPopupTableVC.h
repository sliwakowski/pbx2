//
//  PBPopupTableVC.h
//  Pbx
//
//  Created by Macbook Pro on 05.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBTagsTableVC.h"

@interface PBPopupTableVC : UITableViewController

@property (nonatomic,strong) IBOutlet UILabel* titleLabel;
@property (nonatomic,strong) NSString* kPopupCell;
@property (nonatomic,strong) NSString* keyword;
@property (nonatomic,strong) NSMutableArray* popupListItems;
@property (nonatomic,strong) PBTagsTableVC* tagsTableVC;

@end
