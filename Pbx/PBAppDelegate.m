//
//  PBAppDelegate.m
//  Pbx
//
//  Created by Macbook Pro on 24.04.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBAppDelegate.h"
#import <Parse/Parse.h>
#import "PBUserModel.h"
#import "PBTagsModel.h"
@implementation PBAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [Parse setApplicationId:@"SnK6nIoGQ3Hj5E9XcKw9XtWU3G1N8wB1zik2sP4F"
                  clientKey:@"fzz2DrCNx6rKobRQYLDaxhjsosMu2thjicZZdPAm"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|
     UIRemoteNotificationTypeAlert|
     UIRemoteNotificationTypeSound];
    [PBUserModel setKeywordsForUser];
    
    NSDictionary* userInfo = [launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    if( [apsInfo objectForKey:@"alert"] != NULL)
    {
        [self application:application didReceiveRemoteNotification:userInfo];
    }
    return YES;
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
    NSString *objectId = [userInfo objectForKey:@"objectId"];
    if(![objectId length]<1){
        NSLog(@"Złapałem objectId %@", objectId);
        NSDictionary *articleInfo = [NSDictionary dictionaryWithObject:objectId forKey:@"objectId"];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"OpenSingleArticle" object:nil userInfo:articleInfo];
        /*
        NSDictionary* urlDict = [NSDictionary dictionaryWithObject:[NSURL URLWithString:siteUrl] forKey:@"url"];
        pushWebView = [TCPushWebViewController new];
        pushWebView.siteUrl = siteUrl;
        self.pushWebView.delegate = self;
        [self.window addSubview:pushWebView.view];
         */
        
    }

}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
