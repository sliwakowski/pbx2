//
//  main.m
//  Pbx
//
//  Created by Macbook Pro on 24.04.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PBAppDelegate class]));
    }
}
