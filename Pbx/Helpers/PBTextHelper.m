//
//  PBTextHelper.m
//  Pbx
//
//  Created by Macbook Pro on 13.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import "PBTextHelper.h"

@implementation PBTextHelper

+ (NSString*)clearTextFromString:(NSString*)string {
    NSArray *components = [string componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    NSMutableArray *componentsToKeep = [NSMutableArray array];
    for (int i = 0; i < [components count]; i = i + 2) {
        [componentsToKeep addObject:[components objectAtIndex:i]];
    }
    
    NSString *plainText = [[[componentsToKeep componentsJoinedByString:@""] componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    
    return plainText;
}

@end
