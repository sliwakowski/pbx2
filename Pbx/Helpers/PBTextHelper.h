//
//  PBTextHelper.h
//  Pbx
//
//  Created by Macbook Pro on 13.05.2014.
//  Copyright (c) 2014 The Creators. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBTextHelper : NSObject

+ (NSString*)clearTextFromString:(NSString*)string;

@end
